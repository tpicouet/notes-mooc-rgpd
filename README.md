# Notes Mooc Rgpd

Prise de notes lors du [MOOC RGPD de la CNIL](https://atelier-rgpd.cnil.fr/)

## PDF

Une version compilée est peut-être disponible [ici](https://gitlab.inria.fr/tpicouet/notes-mooc-rgpd/-/jobs/artifacts/master/raw/main.pdf?job=build)
